var gulp = require('gulp'),
	browserSync=require('browser-sync'),
	del=require('del'),
	autoprefixer=require('gulp-autoprefixer'),
	cheerio = require('gulp-cheerio'),
	concat = require('gulp-concat'),
	csscomb = require('gulp-csscomb'),
	csso = require('gulp-csso'),
	notify =require('gulp-notify'),
	pug = require('gulp-pug'),
	replace = require('gulp-replace'),
	sourcemaps = require('gulp-sourcemaps'),
	stylus = require('gulp-stylus'),
	svgSprite = require('gulp-svg-sprite'),
	svgmin = require('gulp-svgmin'),
	tinypng = require('gulp-tinypng'),
	uglifyjs = require('gulp-uglifyjs');

gulp.task('stylus',function () {
	return gulp.src('./dev/static/stylus/main.styl')
		.pipe(sourcemaps.init())
            .pipe(stylus({
                'include css': true
            }))
            .on('error', notify.onError(function (error) {
                return {
                    title: 'Stylus',
                    message: error.message
                };
            }))
            .pipe(sourcemaps.write())
            .pipe(autoprefixer({
                browsers: ['last 5 version']
            }))
            .pipe(gulp.dest('./build/static/css/'))
            .pipe(browserSync.reload({
                stream: true
            }));
})

gulp.task('stylus:build',function () {
	return gulp.src('./dev/static/stylus/main.styl')
            .pipe(stylus({
                'include css': true
            }))            
            .pipe(autoprefixer({
                browsers: ['last 5 version']
            }))
            .pipe(csscomb())
            .pipe(csso())
            .pipe(gulp.dest('./build/static/css/'))
            
})

gulp.task('pug', function () {
	
        return gulp.src('./dev/pug/pages/*.pug')
            .pipe(pug({                
                pretty: true
            }))
            .on('error', notify.onError(function(error) {
                return {
                    title: 'Pug',
                    message: error.message
                };
            }))
            .pipe(gulp.dest('./build/'))
            .on('end',browserSync.reload);
    });

gulp.task('libsJS', function() {
        return gulp.src(['node_modules/svg4everybody/dist/svg4everybody.min.js'])
            .pipe(concat('libs.min.js'))
            .pipe(gulp.dest('./build/static/js/'))
            .pipe(browserSync.reload({
                stream: true
            }));
    });
gulp.task('libsJS:build', function()  {
        return gulp.src(['node_modules/svg4everybody/dist/svg4everybody.min.js'])
            .pipe(concat('libs.min.js'))
            .pipe(uglifyjs())
            .pipe(gulp.dest('./build/static/js/'));
    });
gulp.task('js:copy', function() {
        return gulp.src(['./dev/static/js/*.js',
                           '!./dev/static/js/libs.min.js'])
            .pipe(gulp.dest('./build/static/js/'))
            .pipe(browserSync.reload({
                stream: true
            }));
    });


gulp.task('svg', function() {
        return gulp.src('./dev/static/img/svg/*.svg')
            .pipe(svgmin({
                js2svg: {
                    pretty: true
                }
            }))
            .pipe(cheerio({
                run: function($) {
                    $('[fill]').removeAttr('fill');
                    $('[stroke]').removeAttr('stroke');
                    $('[style]').removeAttr('style');
                },
                parserOptions: { xmlMode: true }
            }))
            .pipe(replace('&gt;', '>'))
            .pipe(svgSprite({
                mode: {
                    symbol: {
                        sprite: "sprite.svg"
                    }
                }
            }))
            .pipe(gulp.dest('./build/static/img/svg/'));
    });

gulp.task('img', function() {
        return gulp.src('./dev/static/img/**/*.{png,jpg,gif}')
            .pipe(gulp.dest('./build/static/img/'));
    });

gulp.task('img:build', function(){
        return gulp.src('./dev/static/img/**/*.{png,jpg,gif}')
            .pipe(tinypng(YOUR_API_KEY))
            .pipe(gulp.dest('./build/static/img/'));
    });


gulp.task('svg:copy', function() {
        return gulp.src('./dev/static/img/general/*.svg')
            .pipe(gulp.dest('./build/static/img/general/'));
    });

gulp.task('fonts', function() {
        return gulp.src('./dev/static/fonts/**/*.*')
            .pipe(gulp.dest('./build/static/fonts/'));
    });

gulp.task('clean', function() {
        return del([
            './build'
        ]);
    });
gulp.task('serve', function() {
        browserSync.init({
            server: './build'
        });
    });

gulp.task('watch', function () {
        gulp.watch('./dev/pug/**/*.pug', gulp.series('pug'));
        gulp.watch('./dev/static/stylus/**/*.styl', gulp.series('stylus'));
        gulp.watch('./dev/static/img/svg/*.svg', gulp.series('svg'));
        gulp.watch('./dev/static/js/**/*.js', gulp.series('libsJS', 'js:copy'));
        gulp.watch(['./dev/static/img/general/**/*.{png,jpg,gif}',
                     './dev/static/img/content/**/*.{png,jpg,gif}'], gulp.series('img'));
    });

gulp.task('dev', gulp.series(
    'clean',
    gulp.series('stylus', 'pug', 'libsJS', 'js:copy', 'svg', 'img', 'fonts','svg:copy'))
);

gulp.task('build', gulp.series(
    'clean',
   gulp.parallel('stylus:build', 'pug', 'libsJS:build', 'js:copy', 'svg', 'img:build', 'fonts','svg:copy')));


gulp.task('default', gulp.series(
    'dev',
    gulp.parallel(
        'watch',
        'serve'
    )
));
